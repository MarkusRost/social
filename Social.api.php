<?php
/**
 * Curse Inc.
 * Social
 * Social API
 *
 * @author		Alex Smith
 * @copyright	(c) 2013 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		Social
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class SocialAPI extends ApiBase {
	/**
	 * Social Settings
	 *
	 * @var		array
	 */
	private $socialSettings = array();

	/**
	 * Hooks Initialized
	 *
	 * @var		boolean
	 */
	private $initialized = false;

	/**
	 * Initiates some needed classes.
	 *
	 * @access	public
	 * @return	void
	 */
	private function init() {
		if (!$this->initialized) {
			global $wgSocialSettings, $wgUser, $wgRequest;

			$this->socialSettings	= $wgSocialSettings;
			$this->wgUser			= $wgUser;
			$this->wgRequest		= $wgRequest;

			$this->DB = wfGetDB(DB_MASTER);

			$this->initialized = true;
		}
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @return	void	[Outputs to screen]
	 */
	public function execute() {
		$this->init();

		$params = $this->extractRequestParams();

		if (intval($params['spid']) < 1) {
			$this->dieUsageMsg(array('invalidspid', $params['spid']));
		} else {
			$this->spid = intval($params['spid']);
		}

		if ($this->wgUser->getId() < 1 && !User::isIP($this->wgUser->getName())) {
			//This should never get triggered since all logged out users have an IP address.  At least, THEY SHOULD!~
			$this->dieUsageMsg(array('invaliduser', $params['do']));
		}
		
		if (!$this->wgUser->getId() && User::isIP($this->wgUser->getName())) {
			$this->user = $this->wgUser->getName();
		} elseif ($this->wgUser->getId()) {
			$this->user = $this->wgUser->getId();
		}

		switch ($params['do']) {
			case 'promptDisable':
				$response = $this->promptDisable();
				break;
			case 'promptTimeout':
				$response = $this->promptTimeout();
				break;
			case 'promptDone':
				$response = $this->promptDone();
				break;
			default:
				$this->dieUsageMsg(array('invaliddo', $params['do']));
				break;
		}

		$apiResult = $this->getResult();
		$apiResult->addValue($params['do'], 'success', $response);
	}

	/**
	 * Requirements for API call parameters.
	 *
	 * @access	public
	 * @return	array	Merged array of parameter requirements.
	 */
	public function getAllowedParams() {
		return [
			'spid' => [
				ApiBase::PARAM_TYPE		=> 'integer',
				ApiBase::PARAM_REQUIRED => true
			],
			'do' => [
				ApiBase::PARAM_TYPE		=> 'string',
				ApiBase::PARAM_REQUIRED => true
			]
		];
	}

	/**
	 * Descriptions for API call parameters.
	 *
	 * @access	public
	 * @return	array	Merged array of parameter descriptions.
	 */
	public function getParamDescription() {
		return [
			'spid'	=> 'The social prompt ID from the database. This is usually provided by the calling AJAX script.',
			'do'	=> 'Action to take on the provided spid and user.'
		];
	}

	/**
	 * Disable Prompts Permanently
	 *
	 * @access	public
	 * @return	boolean Success
	 */
	public function promptDisable() {
		$this->DB->delete('social_prompt', array('user' => $this->user), __METHOD__);
		$total = $this->DB->affectedRows();
		$this->wgRequest->response()->setcookie('noSocialPrompts', 'true', time() + $this->socialSettings['prompt_cookie_time']);

		//Change preference for logged in users.
		if ($this->wgUser->getId() > 0) {
			$this->wgUser->setOption('disable_social_prompts', true);
			$this->wgUser->saveSettings();
		}

		if (!$total) {
			return 'false';
		} else {
			return 'true';
		}
	}

	/**
	 * Set a timeout on the prompts to show up later on.
	 *
	 * @access	public
	 * @return	boolean Success
	 */
	public function promptTimeout() {
		$this->DB->delete('social_prompt', array('user' => $this->user, 'spid' => $this->spid), __METHOD__);
		$total = $this->DB->affectedRows();
		$this->wgRequest->response()->setcookie('noSocialPrompts', 'true', time() + $this->socialSettings['prompt_cookie_time']);

		if (!$total) {
			return 'false';
		} else {
			return 'true';
		}
	}

	/**
	 * Remove the existing prompt entry.
	 *
	 * @access	public
	 * @return	boolean Success
	 */
	public function promptDone() {
		$this->DB->delete('social_prompt', array('user' => $this->user, 'spid' => $this->spid), __METHOD__);
		$total = $this->DB->affectedRows();

		wfRunHooks('CompletedSocialAction', [$this->wgUser]);

		if (!$total) {
			return 'false';
		} else {
			return 'true';
		}
	}

	/**
	 * Get version of this API Extension.
	 *
	 * @access	public
	 * @return	string	API Extension Version
	 */
	public function getVersion() {
		return '1.0';
	}
}
?>